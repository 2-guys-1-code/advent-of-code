const fs = require('fs');
const input = fs.readFileSync('./input.txt').toString().split(',')
const {writeAtPosition, executeProgram}  = require('./02.lib')

//To do this, before running the program, 
//replace position 1 with the value 12 and 
//replace position 2 with the value 2. 
//What value is left at position 0 after the program halts?

let state = [...input]
state = writeAtPosition(state, 1, 12)
state = writeAtPosition(state, 2, 2)

console.log(executeProgram([...state]))