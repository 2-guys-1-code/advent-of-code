const splitIntoChunks = function(memory) {
    const chunk = memory.slice(0, 4)
    return chunk.length
        ? [chunk].concat(splitIntoChunks(memory.slice(4)))
        : memory
}

const loadChunk = function(memory, position) {
    return memory.slice(4 * position, 4 * position + 4)
}

const readAtPosition = function(memory, position) {
    return parseInt(memory[position])
}

const writeAtPosition = function(memory, position, value) {
    return [...memory].slice(0, position).concat([value]).concat([...memory].slice(position+1, memory.length))
}

const executeProgram = (state, pointer = 0) => {
    const chunk = loadChunk(state, pointer)
    const opCode = readAtPosition(chunk, 0)
    const registerA = readAtPosition(chunk, 1)
    const registerB = readAtPosition(chunk, 2)
    const registerC = readAtPosition(chunk, 3)

    if(opCode === 99) {
        return state
    }

    const result = (opCode === 1)
        ? readAtPosition(state, registerA) + readAtPosition(state, registerB)
        : readAtPosition(state, registerA) * readAtPosition(state, registerB)

    const newState = writeAtPosition(state, registerC, result)

    return executeProgram([...newState], pointer+1)
}

module.exports = {splitIntoChunks, loadChunk, readAtPosition, writeAtPosition, executeProgram}