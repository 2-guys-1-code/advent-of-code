const fs = require('fs');
const input = fs.readFileSync('./input.txt').toString().split(',')
const {writeAtPosition, executeProgram}  = require('./02.lib')

let state;
let state0;

for(let noun = 0;noun < 100;noun++){
    for(let verb = 0;verb < 100;verb++){
        state = [...input]
        state = writeAtPosition(state, 1, noun)
        state = writeAtPosition(state, 2, verb)
        
        state = executeProgram([...state])
        state0 = state[0]

        if(state0 === 19690720) {
            console.log({state0, noun, verb})
        }
    }   
}