const { splitIntoChunks, loadChunk, readAtPosition, writeAtPosition, executeProgram } = require('./02.lib')

test('we can split memory into chunks of 4', () => {
    expect(splitIntoChunks([1,2,3,4,5,6,7,8,9,10,11,12])).toStrictEqual([[1,2,3,4], [5,6,7,8],  [9,10,11,12]])
})

test('we can split memory (not dividable by 4) into chunks of 4', () => {
    expect(splitIntoChunks([1,2,3,4,5,6,7,8,9,10,11])).toStrictEqual([[1,2,3,4], [5,6,7,8],  [9,10,11]])
})

test('we want a specific chunk directly', () => {
    expect(loadChunk([1,2,3,4,5,6,7,8,9,10,11], 1)).toStrictEqual([5,6,7,8])
})

test('read memory @ position', () => {
    expect(readAtPosition([1,2,3,4,5,6,7,8,9,10,11], 1)).toBe(2)
})

test('write in memory @ position', () => {
    expect(writeAtPosition([1,2,3,4,5,6,7,8,9,10,11], 1, 20)).toStrictEqual([1,20,3,4,5,6,7,8,9,10,11])
})

test('test program 1', () => {
    expect(executeProgram([1,0,0,0,99])).toStrictEqual([2,0,0,0,99])
})

test('test program 2', () => {
    expect(executeProgram([2,3,0,3,99])).toStrictEqual([2,3,0,6,99])
})

test('test program 3', () => {
    expect(executeProgram([2,4,4,5,99,0])).toStrictEqual([2,4,4,5,99,9801])
})

test('test program 4', () => {
    expect(executeProgram([1,1,1,4,99,5,6,0,99])).toStrictEqual([30,1,1,4,2,5,6,0,99])
})