const {getOrientation, getIntersection, hasIntersection, createSegments, createSegment, createVector, manhattanDist} = require('./03.lib')

test('create vector to right', () => {
    expect(createVector('R75')).toEqual({x: 75, y: 0})
})

test('create vector to left', () => {
    expect(createVector('L25')).toEqual({x: -25, y: 0})
})

test('create vector to down', () => {
    expect(createVector('D25')).toEqual({x: 0, y: -25})
})

test('create vector to up', () => {
    expect(createVector('U4253')).toEqual({x: 0, y: 4253})
})

test('createSegment from origin to right', () => {
    expect(createSegment({x: 0, y: 0}, 'R75')).toEqual({x1: 0, y1: 0, x2: 75, y2: 0})
})

test('createSegment from origin to left', () => {
    expect(createSegment({x: 0, y: 0}, 'L75')).toEqual({x1: 0, y1: 0, x2: -75, y2: 0})
})

test('calculer la distance (a)', () => {
    expect(manhattanDist({x: 0, y: 0}, {x: 1, y: 1})).toBe(2)
})

test('calculer la distance (b)', () => {
    expect(manhattanDist({x: 10, y: 5}, {x: 20, y: 10})).toBe(15)
})

test('createSegments (a)', () => {
    const expected = [
        {x1: 0, y1: 0, x2: 75, y2: 0},
        {x1: 75, y1: 0, x2: 75, y2: -30},
    ]

    expect(createSegments('R75,D30')).toEqual(expected)
})

test('createSegments (b)', () => {
    const expected = [
        {x1: 0, y1: 0, x2: 75, y2: 0},//R75 > x+75
        {x1: 75, y1: 0, x2: 75, y2: -30},//D30 > y-30
        {x1: 75, y1: -30, x2: 158, y2: -30},//R83 > x+83
        {x1: 158, y1: -30, x2: 158, y2: 53},//U83 > y+83
        {x1: 158, y1: 53, x2: 146, y2: 53},//L12 > x-12
        {x1: 146, y1: 53, x2: 146, y2: 4},//D49 > y-49
    ]

    expect(createSegments('R75,D30,R83,U83,L12,D49')).toEqual(expected)
})

test('has intersection', () => {
    const v1 = {x1: 5, y1: 4, x2: 5, y2: 7}
    const v2 = {x1: 4, y1: 6, x2: 8, y2: 6}
    expect(hasIntersection(v1, v2)).toBe(true)
})

test('parallel segments do not cross', () => {
    const v1 = {x1: 5, y1: 4, x2: 5, y2: 7}
    const v2 = {x1: 4, y1: 6, x2: 4, y2: 69}
    expect(hasIntersection(v1, v2)).toBe(false)
})

test('has no intersection', () => {
    const v1 = {x1: 1, y1: 8, x2: 9, y2: 8}
    const v2 = {x1: 4, y1: 3, x2: 4, y2: 6}
    expect(hasIntersection(v1, v2)).toBe(false)
})

test('get orientation (vertical)', () => {
    const v1 = {x1: 5, y1: 4, x2: 5, y2: 7}
    expect(getOrientation(v1)).toBe('vertical')
})


test('get orientation (horizontal)', () => {
    const v1 = {x1: 0, y1: 7, x2: 5, y2: 7}
    expect(getOrientation(v1)).toBe('horizontal')
})

////


test('get intersection', () => {
    const v1 = {x1: 5, y1: 4, x2: 5, y2: 7}
    const v2 = {x1: 4, y1: 6, x2: 8, y2: 6}
    expect(getIntersection(v1, v2)).toEqual({x: 5, y: 6})
})

test('getIntersection : parallel segments do not cross', () => {
    const v1 = {x1: 5, y1: 4, x2: 5, y2: 7}
    const v2 = {x1: 4, y1: 6, x2: 4, y2: 69}
    expect(getIntersection(v1, v2)).toBe(false)
})

test('getIntersection : has no intersection', () => {
    const v1 = {x1: 1, y1: 8, x2: 9, y2: 8}
    const v2 = {x1: 4, y1: 3, x2: 4, y2: 6}
    expect(getIntersection(v1, v2)).toBe(false)
})