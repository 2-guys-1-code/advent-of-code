const fs = require('fs');
//const input = fs.readFileSync('./input-1.txt').toString().split('\n')
const {createSegments, getIntersection, manhattanDist} = require('./03.lib')

const input = [
    'R75,D30,R83,U83,L12,D49,R71,U7,L72',
    'U62,R66,U55,R34,D71,R55,D58,R83'
]

/*
const input = [
    'R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51',
    'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7'
]
*/

const wires = input.filter(wire => wire !== "").map(wire => createSegments(wire));

console.log(wires)

const intersections = []

wires.map( (wireA, indexA) => {
    wires.filter((_, indexB) => indexB > indexA ).map( (wireB, indexB) => {

        wireA.map( wireA => wireB.map(wireB => {
            const intersection = getIntersection(wireA, wireB)
            console.log("test", wireA, "against", wireB, intersection)

            if(intersection !== false) {
                intersections.push(getIntersection(wireA, wireB))
            }

        }))

        //return wiresA.map()
        //return getIntersection(wireA, wireB)
    })  
})

let shortest = Infinity

intersections.map(intersection => {
    const dist = manhattanDist(intersection, {x: 0, y: 0})
    console.log(dist)
    if(dist < shortest) {
        shortest = dist
    }
})



console.log(shortest);