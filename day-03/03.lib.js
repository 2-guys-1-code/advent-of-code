const getOrientation = v => (v.x1 === v.x2) ? 'vertical' : 'horizontal'

const getIntersection = (v1, v2) => {
    if(!hasIntersection(v1, v2)) {
        return false
    }

    if(getOrientation(v1) === 'horizontal') {
        return {x: v2.x1, y: v1.y1}
    } else {
        return {x: v1.x1, y: v2.y1}
    }
}

const hasIntersection = (v1, v2) => {
    if(getOrientation(v1) === getOrientation(v2)) {
        return false
    }

    if(getOrientation(v1) === 'horizontal') {
        return v2.x1 >= v1.x1 && v2.x1 <= v1.x2 && v1.y1 >= v2.y1 && v1.y1 <= v2.y2
    } else {
        return v1.x1 >= v2.x1 && v1.x1 <= v2.x2 && v2.y1 >= v1.y1 && v2.y1 <= v1.y2
    }
}

const createSegments = directions => {
    let start = {x: 0, y: 0}

    return directions.split(',').map(direction => {
        const vector = createVector(direction)
        const end = {
            x: start.x + vector.x, 
            y: start.y + vector.y, 
        }

        const result = {
            x1: start.x,
            y1: start.y,
            x2: end.x,
            y2: end.y,
        }
        
        start = {...end}

        return result;
    })
}

const createSegment = (origin, direction) => { 
    const vector = createVector(direction)

    return {
        x1: origin.x, 
        y1: origin.y, 
        x2: origin.x + vector.x, 
        y2: origin.y + vector.y
    } 
}

const createVector = direction => { 
    const letter = direction.substr(0, 1)
    const length = parseInt(direction.substr(1))
    
    switch(letter) {
        case 'L':
            return {x: -length, y: 0}
            break;

        case 'R':
            return {x: length, y: 0}
            break;

        case 'D':
            return {x: 0, y: -length}
            break;

        case 'U':
            return {x: 0, y: length}
            break;        
    }
}

const manhattanDist = (a, b) => Math.abs(a.x - b.x) + Math.abs(a.y - b.y)

module.exports = {getOrientation, getIntersection, hasIntersection, createSegments, createSegment, createVector, manhattanDist}