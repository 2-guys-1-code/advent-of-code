const pipe = (...fns) => arg => fns.reduce((prev, fn) => fn(prev), arg)

module.exports = { pipe }