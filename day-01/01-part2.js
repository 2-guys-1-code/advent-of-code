const fs = require('fs');
const {pipe} = require('../common')
const {totalFuelRequiredForMass, totalFuel} = require('./01.lib')
const input = fs.readFileSync('./01-input.txt', 'utf-8').split('\n')

const total = pipe(
    masses => masses.map(totalFuelRequiredForMass),
    totalFuel,
)

console.log(total(input))