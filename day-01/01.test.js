const {fuelRequiredForMass, totalFuelRequiredForMass, totalFuelRequiredForMasses, totalFuel} = require('./01.lib');

//
test('0 fuel required for mass of 0', () => {
    expect(fuelRequiredForMass(0)).toBe(0)
})

test('2 fuel required for mass of 12', () => {
    expect(fuelRequiredForMass(12)).toBe(2)
})

test('2 fuel required for mass of 14', () => {
    expect(fuelRequiredForMass(14)).toBe(2)
})

test('654 fuel required for mass of 1969', () => {
    expect(fuelRequiredForMass(1969)).toBe(654)
})

test('33583 fuel required for mass of 100756', () => {
    expect(fuelRequiredForMass(100756)).toBe(33583)
})

//

test('calculate reduce result for fuel 2 and 654', () =>{
    expect(totalFuel([2, 654])).toBe(656)
})

test('calculate reduce result for fuel 10, 970 and 20', () =>{
    expect(totalFuel([10, 970, 20])).toBe(1000)
})

// ------ part 2

test('2 fuel (total) required for mass 14', () => {
    expect(totalFuelRequiredForMass(14)).toBe(2)
})

test('966 fuel (total) required for mass 1969', () => {
    expect(totalFuelRequiredForMass(1969)).toBe(966)
})

test('50346 fuel (total) required for mass 100756', () => {
    expect(totalFuelRequiredForMass(100756)).toBe(50346)
})
