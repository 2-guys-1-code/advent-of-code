const {pipe} = require('../common')

const fuelRequiredForMass = mass => Math.max(0, Math.floor(mass / 3) - 2)
const totalFuel = values => values.reduce((total, current) => current + total, 0)
const allFuels = (result = []) => mass => {
    const fuel = fuelRequiredForMass(mass)
    return mass > 0
        ? allFuels([...result, fuel])(fuel)
        : result
}
const totalFuelRequiredForMass = pipe(allFuels(), totalFuel)

module.exports = {fuelRequiredForMass, totalFuel, totalFuelRequiredForMass}
