const fs = require('fs');
const {pipe} = require('../common')
const {fuelRequiredForMass, totalFuel} = require('./01.lib')
const input = fs.readFileSync('./01-input.txt', 'utf-8').split('\n')

const total = pipe(
    masses => masses.map(fuelRequiredForMass),
    totalFuel,
)

console.log(total(input))